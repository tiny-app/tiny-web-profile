import React, { useEffect } from 'react';
import WOW from 'wowjs';

const About = () => {

  useEffect(() => {
    new WOW.WOW({
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 20,
      mobile: true,
      live: false,
    }).init();
  }, [])
  
  return (
    <div id="section-2" className="container-fluid d-flex align-items-center px-0">
      <div className="container my-3 my-lg-0">
        <div className="row">
          <div className="col col-12 col-md-6 d-none d-md-block">
            <img
              src={require('../../assets/img/bg/bg3.svg')} alt="illustration"
              className="w-100 wow fadeInLeft"
            />
          </div>
          <div className="col col-12 col-md-6">
            <div className="jumbotron bg-transparent mx-0 px-0">
              <h1 className="display-4 text-dark text-left">About <span className="text-header">Us</span></h1>
              <br className="my-5" />
              <p className="font-abel text-muted text-left">
                Berdiri sejak akhir tahun 2019. Beranggota-kan dari ketiga pemuda yang ingin mencoba menggebrak. Dan Tinystudio muncul untuk menjawab semua kebutuhan di bidang IT.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default About