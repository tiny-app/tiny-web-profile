import React, { useEffect } from 'react';
import { handleNav } from '../helper.js';
import WOW from 'wowjs';

const Banner = () => {

  useEffect(() => {
    new WOW.WOW({
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 20,
      mobile: true,
      live: false,
    }).init();
  }, [])

  return (
    <div id="section-1" className="container-fluid">
      <div className="container">
        {/* Shape */}
        <div className="shape-container">
          <img src={require('../../assets/img/shape/shape1.svg')} alt="shape" className="shape shape1" />
          <img src={require('../../assets/img/shape/shape2.svg')} alt="shape" className="shape shape2" />
          <img src={require('../../assets/img/shape/shape3.svg')} alt="shape" className="shape shape3" />
          <img src={require('../../assets/img/shape/shape4.svg')} alt="shape" className="shape shape4" />
        </div>
        {/* End Shape */}
        <div className="row d-flex align-items-center">
          <div className="col col-12 col-md-6">
            <div className="jumbotron bg-transparent m-0 p-0">
              <h1 className="display-4 text-dark">You imagine, <br/>we make it happen.</h1>
              <p className="font-abel">Memenuhi segala kebutuhan anda dengan resource yang kami punya dengan sepenuh hati.</p>
              <button className="btn btn-get-in-touch" onClick={() => handleNav('section-2')} nohref="true">Get in touch</button>
            </div>
          </div>
          <div className="col col-12 col-md-6 mt-5 mt-md-0">
            <img
              src={require('../../assets/img/bg/bg1.svg')} alt="illustration"
              className="w-100 wow fadeInRight"
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Banner