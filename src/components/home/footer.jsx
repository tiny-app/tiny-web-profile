import React from 'react';

const Footer = () => (
  <div id="section-6" className="container-fluid bg-light">
    <div className="container py-5">
      <div className="row">
        <div className="col-lg-4 col-md-6">
          <div className="footer-widget">
            <img
              src={require('../../assets/img/logo.svg')}
              style={{
                width: '200px'
              }}
              alt="our-logo" id="logo"
              className="mb-3"
            />
            <p className="text-muted">Memenuhi segala kebutuhan anda dengan resource yang kami punya dengan sepenuh hati.</p>
          </div>
        </div>
        <div className="col-lg-4 col-md-6 mb-3 mb-md-0">
          <div className="footer-widget pl-0 pl-lg-5">
            <h4>Contact Us</h4>
            <ul className="list">
              <li className="text-muted">
                <i className="fa fa-phone mr-2" />
                +62 831 5394 1808
              </li>
              <li className="text-muted">
                <i className="fa fa-envelope-o mr-2" />
                tinystudioo4@gmail.com
              </li>
            </ul>
          </div>
        </div>
        <div className="col-lg-4 col-md-6">
          <div className="footer-widget">
            <h4>Our Address</h4>
            <p className="text-muted">Jl. Markisa No. 4 Kota Samarinda, Kalimantan Timur</p>
          </div>
        </div>
      </div>
    </div>
    <div className="copyright bg-white p-3">
      <i className="fa fa-copyright" /> Copyright 2020 Tinystudioo.
    </div>
  </div>
)

export default Footer