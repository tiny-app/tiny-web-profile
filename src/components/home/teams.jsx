import React, { useEffect } from 'react';
import WOW from 'wowjs';

const Teams = () => {

  useEffect(() => {
    new WOW.WOW({
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 20,
      mobile: true,
      live: false,
    }).init();
  }, [])

  return (
    <div id="section-5" className="container-fluid d-flex align-items-center text-center px-0">
      <div className="container my-5">
        <h1 className="display-4 text-dark mb-5">Meet <span className="text-header">our team</span></h1>
        <div className="row">
          <div className="col mx-2 mb-5 mb-lg-0">
            <div className="card border-0 bg-white team-card wow rotateInUpLeft">
              <div className="card-body">
                <img src={require("../../assets/img/profile/difa.svg")} alt="Profile" />
              </div>
              <div className="card-footer border-0 bg-transparent">
                <h4>Difa Ananda</h4>
                <p className="font-abel">FRONTEND DEVELOPER</p>
                <a href="#difa-fb">
                  <img src={require('../../assets/img/icons/icon-fb.svg')} alt="FB" />
                </a>
                <a href="#difa-ig">
                  <img src={require('../../assets/img/icons/icon-ig.svg')}  alt="IG" />
                </a>
              </div>
            </div>
          </div>
          <div className="col mx-2 mb-5 mb-lg-0">
            <div className="card border-0 bg-white team-card wow slideInUp">
              <div className="card-body">
                <img src={require("../../assets/img/profile/rois.svg")} alt="Profile" />
              </div>
              <div className="card-footer border-0 bg-transparent">
                <h4>Rois Maulana</h4>
                <p className="font-abel">UI/UX DESIGNER</p>
                <a href="#difa-fb">
                  <img src={require('../../assets/img/icons/icon-fb.svg')} alt="FB" />
                </a>
                <a href="#difa-ig">
                  <img src={require('../../assets/img/icons/icon-ig.svg')}  alt="IG" />
                </a>
              </div>
            </div>
          </div>
          <div className="col mx-2 mb-5 mb-lg-0">
            <div className="card border-0 bg-white team-card wow rotateInUpRight">
              <div className="card-body">
                <img src={require("../../assets/img/profile/julian.svg")} alt="Profile" />
              </div>
              <div className="card-footer border-0 bg-transparent">
                <h4>Julian Effendi</h4>
                <p className="font-abel">BACKEND DEVELOPER</p>
                <a href="#difa-fb">
                  <img src={require('../../assets/img/icons/icon-fb.svg')} alt="FB" />
                </a>
                <a href="#difa-ig">
                  <img src={require('../../assets/img/icons/icon-ig.svg')}  alt="IG" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Teams