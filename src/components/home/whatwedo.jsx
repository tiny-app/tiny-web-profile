import React, { useEffect } from 'react';
import WOW from 'wowjs';

const WhatWeDo = () => {
  
  useEffect(() => {
    new WOW.WOW({
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 20,
      mobile: true,
      live: false,
    }).init();
  }, [])

  return (
    <div id="section-3" className="container-fluid d-flex align-items-center text-center px-0">
      <div className="container my-5">
        <h1 className="display-4 text-dark mb-5">What <span className="text-header">we do</span></h1>
        <div className="row">
          <div className="col mb-5 mb-xl-0">
            <div className="card border-0 wow fadeInUpBig">
              <div className="card-body">
                <img src={require('../../assets/img/icons/icon-web-development.svg')} alt="Web Development" />
              </div>
              <div className="card-footer border-0 bg-transparent">
                <h5>WEB DEVELOPMENT</h5>
                <p className="font-abel text-muted">
                  Membuat dan mengembangkan website mulai dari nol, sesuai dengan kebutuhan client.
                </p>
              </div>
            </div>
          </div>
          <div className="col mb-5 mb-xl-0">
            <div className="card border-0 wow fadeInUpBig" data-wow-delay="0.2s">
              <div className="card-body">
                <img src={require('../../assets/img/icons/icon-mobile-app.svg')} alt="Mobile App" />
              </div>
              <div className="card-footer border-0 bg-transparent">
                <h5>MOBILE APP</h5>
                <p className="font-abel text-muted">Bertransformasi dari yang menggunakan sistem berbasis komputer agar bisa dioperasi-kan hanya melalui genggaman.</p>
              </div>
            </div>
          </div>
          <div className="col mb-5 mb-xl-0">
            <div className="card border-0 wow fadeInUpBig" data-wow-delay="0.4s">
              <div className="card-body">
                <img src={require('../../assets/img/icons/icon-ui-ux-design.svg')} alt="UI/UX" />
              </div>
              <div className="card-footer border-0 bg-transparent">
                <h5>UI/UX DESIGNER</h5>
                <p className="font-abel text-muted">Merancang tampilan antarmuka untuk memudahkan user mengoperasi-kan sebuah aplikasi.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default WhatWeDo