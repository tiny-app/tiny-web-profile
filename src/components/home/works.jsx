import React, { useState, useEffect } from 'react';
import OwlCarousel from 'react-owl-carousel3';
import {
  Link
} from 'react-router-dom';
import axios from '../../customAxios.js';
import {
  Skeleton
} from 'antd';
import {
  Card
} from 'react-bootstrap';
import WOW from 'wowjs';

const Works = () => {

  const [ data, setData ] = useState([])
  const [ isLoaded, setIsLoaded ] = useState(false)

  useEffect(() => {
    new WOW.WOW({
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 20,
      mobile: true,
      live: false,
    }).init();

    const fetchData = () => {
      axios.get(`/profile/project`)
      .then(res => {
        setData(res.data.data.data)
        setIsLoaded(true)
      })
    }
    fetchData()
  }, [])

  const options = { 
    autoplay: true,
    nav: true,
    loop:true,
    mouseDrag: true,
    autoplayHoverPause: true,
    responsiveClass: true,
    dots: false,
    navText: [
        "<i class='navigate-left'>< Prev</i>",
        "<i class='navigate-right'>Next ></i>"
    ],
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:2,
        },
        1200:{
            items:3,
        }
    },
    lazyLoad:true,
    autoplayTimeout: 3000,
  }

  const SkeletonLoading = () => (
    <>
      <div className="col">
        <Card className="border-0 bg-white p-3 shadow-sm"><Skeleton active /></Card>
      </div>
      <div className="col">
        <Card className="border-0 bg-white p-3 shadow-sm"><Skeleton active /></Card>
      </div>
      <div className="col">
        <Card className="border-0 bg-white p-3 shadow-sm"><Skeleton active /></Card>
      </div>
    </>
  )

  return (
    <div id="section-4" className="container-fluid d-flex align-items-center text-center px-0">
      <div className="container my-5">
        <h1 className="display-4 text-dark mb-5">Our <span className="text-header">work</span></h1>
        <div className="row wow fadeInUpBig">
        {
          !isLoaded
          ? <SkeletonLoading />
          : <OwlCarousel 
              className="owl-theme py-5"
              {...options}
            >
              {
                data.map((dt, index) => (
                  <div className="col mx-2 mb-5 mb-lg-0" key={index}>
                    <div className="card border-0 bg-white portofolio-card shadow-sm">
                      <div className="card-body p-0">
                        <img src={dt.thumbnail_url} alt="PORTOFOLIO" className="portofolio-img" />
                        <Link to={`/portofolio/${dt.id}`} className="show link fa fa-eye" style={{
                          fontSize: '25px'
                        }}></Link>
                      </div>
                      <div className="card-footer border-0 bg-white p-4">
                        <h5>{dt.title}</h5>
                        <p className="font-abel text-muted desc">{dt.description}</p>
                      </div>
                    </div>
                  </div>
                  ))
              }
            </OwlCarousel>
        }
        </div>
      </div>
    </div>
  )
}

export default Works