import React, { useState, useEffect } from 'react';
import {
  Navbar,
  Nav,
  Container,
} from 'react-bootstrap';
import { handleNav } from './helper.js';

const NavbarComponent = () => {

  const style = {
    style1: 'navbar bg-transparent navbar-light position-absolute mt-4',
    style2: 'navbar bg-light navbar-light shadow-sm fixed-top p-3'
  }

  const [ isScrolled, setIsScrolled ] = useState(false)
  
  useEffect(() => {

    const width = window.innerWidth;
    if(width < 576) setIsScrolled(true)

    window.addEventListener('scroll', () => {
      const width = window.innerWidth;
      if(width > 576) {
        const currentScroll = window.scrollY;
        const navHeight = 80;
        if(currentScroll > navHeight) {
          setIsScrolled(true)
        } else setIsScrolled(false)
      }
    })

    window.addEventListener('resize', () => {
      const width = window.innerWidth;
      if(width < 576) {
        setIsScrolled(true)
      } else setIsScrolled(false)
    })
  }, [])

  return (
    <Navbar id="navbar" expand="lg" className={!isScrolled ? style.style1 : style.style2}>
      <Container>
        <Navbar.Brand href="#section-1">
          <img src={require('../assets/img/logo.svg')} alt="our-logo" id="logo"/>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="navbar-collapse" />
        <Navbar.Collapse id="navbar-collapse" className="p-0">
          <Nav className="ml-auto">
            <Nav.Link onClick={() => handleNav('section-1')}>HOME</Nav.Link>
            <Nav.Link onClick={() => handleNav('section-2')}>ABOUT US</Nav.Link>
            <Nav.Link onClick={() => handleNav('section-3')}>WHAT WE DO</Nav.Link>
            <Nav.Link onClick={() => handleNav('section-4')}>OUR WORK</Nav.Link>
            <Nav.Link onClick={() => handleNav('section-5')}>OUR TEAM</Nav.Link>
            <Nav.Link onClick={() => handleNav('section-6')}>CONTACT US</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default NavbarComponent