import React from 'react';
import {
  Link
} from 'react-router-dom';

const Navbar = () => {

  const style = 'navbar navbar-expand-lg bg-light navbar-light shadow-sm p-3';

  return (
    <nav id="navbar" className={style}>
      <div className="container">
        <Link to="/" className="navbar-brand">
          <img src={require('../assets/img/logo.svg')} alt="our-logo" id="logo"/>
        </Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbar-collapse">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <Link to="/" className="nav-link">HOME</Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Navbar