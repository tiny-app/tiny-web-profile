import React from 'react';

const Label = ({ text }) => (
  <div className="label">
    #{text}
  </div>
)

export default Label