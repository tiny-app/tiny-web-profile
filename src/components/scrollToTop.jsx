import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';

function ScrollToTop({ history }) {
  const [ showBtn, setShowBtn ] = useState(false);

  useEffect(() => {
    const unlisten = history.listen(() => {
      window.scrollTo(0, 0)
    });

    const path = history.location.pathname;
    if(path === "/" || path.includes('/portofolio')) {
      setShowBtn(true)
    }

    if(showBtn) {
      window.addEventListener('scroll', () => {
        const currentScroll = window.scrollY;
        const btnBackToTop = document.getElementById('btn-back-to-top');
        if(currentScroll > 80) {
          btnBackToTop.style.display = 'block'
        } else btnBackToTop.style.display = 'none'
      })
    }

    return () => {
      unlisten();
    }
  }, [history, showBtn]);

  const scrollTop = () => {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: "smooth"
    });
  };

  return (
    <button className="btn btn-back-to-top" id="btn-back-to-top" onClick={scrollTop}>
      Back to top
      <i className="fa fa-arrow-up ml-2" />
    </button>
  );
}

export default withRouter(ScrollToTop);