import React, { Fragment } from 'react';
import {
  Route,
  withRouter,
} from 'react-router-dom';
import { AnimatedSwitch } from "./components/animatedSwitch";
import { Helmet } from 'react-helmet';

import Home from './pages/home.jsx';
import Portofolio from './pages/portofolio.jsx';
import NoMatch from './pages/404.jsx';

const Container = ({ location }) => (
  <Fragment>
    <Helmet>
      <meta name="description" content="Tiny Studioo is a software house based in Samarinda, Indonesia which provides a solution for developing a web and mobile app for your personal or company needs." />
    </Helmet>
    <AnimatedSwitch location={location}>
      <Route exact path="/" component={Home} />
      <Route path="/portofolio/:id" component={Portofolio} />
      <Route component={NoMatch} />
    </AnimatedSwitch>
  </Fragment>
)

export default withRouter(Container)