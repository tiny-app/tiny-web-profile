import React from 'react';
import {
  Link
} from 'react-router-dom';

const NoMatch = () => (
  <div id="no-match">
    <h1>404</h1>
    <p>Ooopsss... Page not found!</p>
    <Link to="/" className="btn btn-go-home">Go Home</Link>
  </div>
)

export default NoMatch