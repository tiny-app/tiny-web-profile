import React from 'react';
import '../assets/scss/style.scss';
import Navbar from '../components/navbar';
import Banner from '../components/home/banner';
import About from '../components/home/about';
import Works from '../components/home/works';
import WhatWeDo from '../components/home/whatwedo';
import Teams from '../components/home/teams';
import Footer from '../components/home/footer';

const Home = () => {
  return (
    <div id="main-content" className="container-fluid p-0">
      <Navbar />
      <Banner />
      <About />
      <WhatWeDo />
      <Works />
      <Teams />
      <Footer />
    </div>
  )
}

export default Home