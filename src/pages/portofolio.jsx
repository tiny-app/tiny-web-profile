import React, { useState, useEffect } from 'react';
import Navbar from '../components/navbarSecond';
import '../assets/scss/portofolio.scss';
import {
  Link
} from 'react-router-dom';

import {
  Skeleton,
} from 'antd';

import axios from '../customAxios.js';
import Carousel, { Modal, ModalGateway } from 'react-images';
import Label from '../components/portofolio/label';

const Portofolio = (props) => {
  
  const id = props.match.params.id;

  const [ data, setData ] = useState([])
  const [ isLoaded, setIsLoaded ] = useState(false)
  const [ imgState, setImgState ] = useState({
    selectedIndex: 0,
    lightboxIsOpen: false
  })

  const toggleLightBox = selectedIndex => {
    setImgState({
      lightboxIsOpen: !imgState.lightboxIsOpen,
      selectedIndex
    })
  }

  useEffect(() => {
    const fetchData = () => {
      axios.get(`/profile/project/${id}`)
      .then(res => {
        const data = res.data.data
        let files = [];
        data.files.forEach(file => {
          files = [...files, { src: file.file_url }]
        })
        let technologies = [];
        data.frameworks.length >= 1 && data.frameworks.forEach(dt => technologies = [...technologies, dt.name])
        data.languages.length >= 1 && data.languages.forEach(dt => technologies = [...technologies, dt.name])
        setData({
          ...data,
          files,
          technologies
        })
        setIsLoaded(true)
      })
    }
    fetchData()
  }, [id])

  return (
    <div id="main-content" className="container-fluid p-0">
      <Navbar />
      <div className="container py-4">
        <Link to="/" className="font-abel font-weight-bold back-btn">
          <i className="fa fa-long-arrow-left mr-1" />
          Back
        </Link>
        <h4 className="my-4">{data.title}</h4>
          {
            !isLoaded
            ? <div className="row">
                <div className="col">
                  <div className="card border-0 shadow-sm p-3 mb-3">
                    <Skeleton active />
                  </div>
                  <div className="card border-0 shadow-sm p-3 mb-3">
                    <Skeleton active />
                  </div>
                </div>
              </div>
            : <>
                <div className="row">
                  <div className="col col-12 img-container pb-4">
                    {
                      data.files.length >= 1
                      ? <img className="shadow-sm mw-100 rounded" src={data.files[0].src} alt="fullpic" />
                      : <p>There is no image for now.</p>
                    }
                  </div>
                </div>
                {
                  data.demo_link &&
                  <div className="row">
                    <div className="col col-12 pb-4">
                      <a href={data.demo_link} rel="noopener noreferrer" className="font-abel visit-btn">
                        <i className="fa fa-external-link mr-2" />
                        VISIT SITE
                      </a>
                    </div>
                  </div>
                }
                <div className="row">
                  <div className="col col-auto technologies pb-4">
                  {
                    data.technologies.map((text, index) => <Label text={text} key={index} />)
                  }
                  </div>
                </div>
                <div className="row">
                  <div className="col col-12 about pb-4">
                    <h5>About this project</h5>
                    <p className="font-abel text-muted text-justify">
                      {data.description}
                    </p>
                  </div>
                </div>
                {
                  data.files.length >= 1 &&
                  <div className="row">
                    <div className="col col-12 screenshoot">
                      <h5>Screenshoot</h5>
                      <div className="row">
                        <div className="col col-12">
                          <div className="card-columns">
                          {
                            data.files.map((file, index) => (
                              <div className="img-card card border-0" key={index}>
                                <img className="shadow-sm rounded" src={file.src} alt="fullpic" />
                                <i className="show link fa fa-eye" onClick={() => toggleLightBox(index)} style={{ fontSize: '25px' }} />
                              </div>
                            ))
                          }
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                }
              </>
          }
      </div>
      <ModalGateway>
        {imgState.lightboxIsOpen ? (
          <Modal onClose={toggleLightBox}>
            <Carousel
              currentIndex={imgState.selectedIndex}
              views={data.files}
            />
          </Modal>
        ) : null}
      </ModalGateway>
    </div>
  )
}

export default Portofolio